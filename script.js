Array.prototype.sample = function () {
    return this[Math.floor(predictableRandom() * this.length)]
}
Array.prototype.difference = function (arr2) {
    return this.filter((x) => !arr2.includes(x))
}
Set.prototype.filter = function (fn) {
    return new Set([...this].filter(fn))
}
Set.prototype.sample = function () {
    return [...this].sample()
}
if (Set.prototype.difference === undefined) {
    Set.prototype.difference = function (set2) {
        const setA = new Set(this)

        for (const v of set2.values()) {
            setA.delete(v)
        }

        return setA
    }
}
String.prototype.escapeHtml = function () {
    return this.replaceAll("&", "&amp;")
        .replaceAll("<", "&lt;")
        .replaceAll(">", "&gt;")
        .replaceAll('"', "&quot;")
        .replaceAll("'", "&#039;")
}
function mod(n, d) {
    return ((n % d) + d) % d
}

function hashCode(str) {
    return str
        .split("")
        .reduce(
            (prevHash, currVal) =>
                ((prevHash << 5) - prevHash + currVal.charCodeAt(0)) | 0,
            0,
        )
}
var randomSeed = 1
function setSeed(str) {
    randomSeed = hashCode(str)
}
function predictableRandom() {
    var x = Math.sin(randomSeed++) * 10000
    return x - Math.floor(x)
}

const emoji = {
    stringMatch: "🟩",
    stringNoMatch: "🟥",
    regexWrong: "🤔",
    regexCorrect: "🎉",
    gaveUp: "🤷",
}

const stringHistory = {
    guesses: [],
    index: -1,
    latest: "",
}

const regexHistory = {
    guesses: [],
    index: -1,
    latest: "",
}

let emojiHistory = ""

let exercises = ["two|three|five|seven"]

let stringGuesses = 0
let regexGuesses = 0

let lastGuess

document.addEventListener(
    "DOMContentLoaded",
    function (event) {
        /*
        let all_puzzles_work =
            PUZZLES.map((x) => new RegExp(x)).findIndex(
                (x) =>
                    english.every((word) => word.match(x)) ||
                    english.every((word) => !word.match(x)),
            ) === -1
        console.log("All puzzles work:", all_puzzles_work)
        */

        const strings = new Set(english)

        const latestdiv = document.querySelector("#latest")
        const guessBox = document.querySelector("#guess")
        const submitStringButton = document.querySelector("#submit-string")
        const submitRegexButton = document.querySelector("#submit-regex")
        const ruleGuessBox = document.querySelector("#rule-guess")
        const newPatternBox = document.querySelector("#new-pattern")
        const newPatternButton = document.querySelector("#new-pattern-button")
        const matchingAnswerdiv = document.querySelector("#matching")
        const nonmatchingAnswerdiv = document.querySelector("#non-matching")
        const regexGuessesdiv = document.querySelector("#guesses")
        const closePopupButton = document.querySelector("#close-button")
        const openPopupButton = document.querySelector("#guess-pattern-button")
        const giveUpButton = document.querySelector("#give-up-button")

        window.onkeydown = (event) => {
            if (event.key === "Escape") {
                closePopup()
            }
        }

        submitStringButton.addEventListener("click", submit)
        submitRegexButton.addEventListener("click", submitRule)

        guessBox.onkeydown = (event) => {
            if (event.key === "Enter") {
                submit()
            } else if (event.key === "ArrowUp") {
                event.preventDefault()
                scrollHistory(-1, stringHistory, guessBox)
            } else if (event.key === "ArrowDown") {
                event.preventDefault()
                scrollHistory(1, stringHistory, guessBox)
            }
        }

        ruleGuessBox.onkeydown = (event) => {
            if (event.key === "Enter") {
                submitRule()
            } else if (event.key === "ArrowUp") {
                event.preventDefault()
                scrollHistory(-1, regexHistory, ruleGuessBox)
            } else if (event.key === "ArrowDown") {
                event.preventDefault()
                scrollHistory(1, regexHistory, ruleGuessBox)
            }
        }
        closePopupButton.onclick = (event) => {
            closePopup()
        }
        openPopupButton.onclick = (event) => {
            openPopup()
        }
        giveUpButton.onclick = (event) => {
            giveUp()
        }
        newPatternButton.onclick = (event) => {
            createNewPattern()
        }
        newPatternBox.onkeydown = (event) => {
            if (event.key === "Enter") {
                createNewPattern()
            }
        }
        const queryString = window.location.search.substring(1)
        let regexpattern = decodeURIComponent(atob(queryString))
        let regexpatternAsRegex

        try {
            regexpatternAsRegex = new RegExp(regexpattern)
        } catch (e) {
            alert(
                `${e.message.escapeHtml()}. We will redirect you to the default regular expression.`,
            )
            window.location.href = window.location.origin
            return
        }

        const usingDailyRegex = !queryString
        let day
        if (!queryString) {
            const now = new Date()
            const start = new Date(2024, 4, 15)
            const timeDiff = now.getTime() - start.getTime()
            day = Math.ceil(timeDiff / (1000 * 60 * 60 * 24))
            puzzleIndex = mod(day - 1, PUZZLES.length)
            regexpattern = PUZZLES[puzzleIndex]
        }

        setSeed(regexpattern)

        let matchingStrings = strings.filter(
            (w) => w.match(regexpattern) !== null,
        )
        let nonmatchingStrings = strings.filter(
            (w) => w.match(regexpattern) === null,
        )

        if (matchingStrings.size === 0) {
            alert(
                "The specified pattern does not match any words in our dictionary. This will not be a fun game :/ We will redirect you to the default regular expression.",
            )
            window.location.href = window.location.origin
            return
        }
        if (nonmatchingStrings.size === 0) {
            alert(
                "The specified pattern matches all words in our dictionary. This will not be a fun game :/ We will redirect you to the default regular expression.",
            )
            window.location.href = window.location.origin
            return
        }

        guessBox.focus()

        // Give two starting examples.
        matchingAnswerdiv.innerHTML = matchingStrings.sample() + "<br>"
        nonmatchingAnswerdiv.innerHTML = nonmatchingStrings.sample() + "<br>"

        function normalizeRegex(regex) {
            return regex.replaceAll("/", "")
        }

        function submitRule() {
            lastGuess = normalizeRegex(ruleGuessBox.value)
            regexGuessesdiv.innerHTML = `${lastGuess.escapeHtml()}<br>${regexGuessesdiv.innerHTML}`

            regexHistory.guesses.unshift(lastGuess)
            regexHistory.index = -1
            ruleGuessBox.lastGuess = ""
            let guessPattern

            regexGuesses += 1

            try {
                guessPattern = new RegExp(lastGuess)
            } catch (e) {
                latestdiv.classList = "complaining"
                latestdiv.innerHTML = e.message
                closePopup()
                return
            }
            let guessMatchingStrings = strings.filter(
                (w) => w.match(guessPattern) !== null,
            )
            let guessNonmatchingStrings = strings.filter(
                (w) => w.match(guessPattern) === null,
            )
            let matchingUnexpectedly =
                matchingStrings.difference(guessMatchingStrings) // das sind alle zutreffenden, ohne die, bei denen das Spiely schon vermutet, dass sie zutreffen
            //These are all the correct ones, excluding those that the game already suspects are correct
            let nonmatchingUnexpectedly = nonmatchingStrings.difference(
                guessNonmatchingStrings,
            ) // das sind alle nicht-zutreffenden, ohne die, bei denen es schon vermutet, dass sie nicht zutreffen
            //These are all the non-applicable ones, excluding those for which it is already suspected that they do not apply

            //console.log("whole sets", guessMatchingStrings, guessNonmatchingStrings)
            //console.log("differences", matchingUnexpectedly, nonmatchingUnexpectedly)

            if (
                matchingUnexpectedly.size === 0 &&
                nonmatchingUnexpectedly.size === 0
            ) {
                endGame(true)
            } else {
                if (nonmatchingUnexpectedly.size > 0) {
                    const adverseExample = nonmatchingUnexpectedly.sample()
                    latestdiv.classList = "complaining"
                    latestdiv.innerHTML = `Incorrect! According to your guess, <code>${adverseExample.escapeHtml()}</code> would match the pattern, but it does not.`
                    stringHistory.guesses.unshift(adverseExample)
                    nonmatchingAnswerdiv.innerHTML = `${adverseExample}<br>${nonmatchingAnswerdiv.innerHTML}`
                } else {
                    const adverseExample = matchingUnexpectedly.sample()
                    latestdiv.classList = "complaining"
                    latestdiv.innerHTML = `Incorrect! According to your guess, <code>${adverseExample.escapeHtml()}</code> would not match the pattern, but it does.`
                    stringHistory.guesses.unshift(adverseExample)
                    matchingAnswerdiv.innerHTML = `${adverseExample}<br>${matchingAnswerdiv.innerHTML}`
                }
                emojiHistory += emoji.regexWrong
            }
            closePopup()
        }

        function submit() {
            const value = guessBox.value
            guessBox.value = ""

            if (
                couldBeRegex(value) &&
                confirm(
                    "The value looks like a regex. Submit this string as a regex guess?",
                )
            ) {
                ruleGuessBox.value = value
                return submitRule()
            }

            stringHistory.guesses.unshift(value)
            stringHistory.index = -1
            guessBox.value = ""

            stringGuesses += 1

            let ergebnis = value.match(regexpattern)
            if (ergebnis !== null) {
                let antwort = `${value.escapeHtml()}<br>`
                matchingAnswerdiv.innerHTML =
                    antwort + matchingAnswerdiv.innerHTML
                latestdiv.classList = "affirmative"
                latestdiv.innerHTML = `The string <code>${value.escapeHtml()}</code> matches the secret pattern.`
                emojiHistory += emoji.stringMatch
            } else {
                let antwort = `${value.escapeHtml()}<br>`
                nonmatchingAnswerdiv.innerHTML =
                    antwort + nonmatchingAnswerdiv.innerHTML
                latestdiv.classList = "complaining"
                latestdiv.innerHTML = `The string <code>${value.escapeHtml()}</code> does <b>not</b> match the secret pattern.`
                emojiHistory += emoji.stringNoMatch
            }
        }
        function encodeRegex(regex) {
            let uriEncodedPattern = encodeURIComponent(regex)
            let encodedPattern64 = btoa(uriEncodedPattern)
            return encodedPattern64
        }

        function createNewPattern() {
            let pattern = normalizeRegex(newPatternBox.value)
            if (pattern !== "") {
                location = "?" + encodeRegex(pattern)
            }
        }
        function scrollHistory(direction, history, inputBox) {
            if (history.guesses.length === 0) {
                return
            }
            if (history.index === -1) {
                // Remember the latest value before scrolling away
                history.latest = inputBox.value
            }

            history.index -= direction
            if (history.index < -1) {
                history.index = -1
            }
            if (history.index > history.guesses.length - 1) {
                history.index = history.guesses.length - 1
            }
            if (history.index === -1) {
                inputBox.value = history.latest
            } else {
                inputBox.value = history.guesses[history.index]
            }
            inputBox.selectionStart = inputBox.selectionEnd =
                inputBox.value.length
            inputBox.scrollLeft = inputBox.scrollWidth
        }
        function closePopup() {
            document.querySelector("#guess-popup").style.display = "none"
            guessBox.focus()
        }
        function openPopup() {
            document.querySelector("#guess-popup").style.display = "flex"
            ruleGuessBox.focus()
        }
        function giveUp() {
            if (
                confirm(
                    "Are you sure you want to give up and see the solution?",
                )
            ) {
                endGame(false)
            }
        }
        function endGame(win) {
            guessBox.disabled = true
            openPopupButton.disabled = true
            giveUpButton.disabled = true

            const string = `${stringHistory.guesses.length} string${stringHistory.guesses.length !== 1 ? "s" : ""}`
            const regex = `${regexHistory.guesses.length} regex${regexHistory.guesses.length !== 1 ? "es" : ""}`

            let puzzleNumber = puzzleIndex + 1

            if (win) {
                latestdiv.classList = "affirmative"
                const daily = usingDailyRegex ? "today's" : "the"
                if (ruleGuessBox.value === regexpattern) {
                    latestdiv.innerHTML = `<b>Correct! <code>${regexpattern.escapeHtml()}</code> is ${daily} secret regex!</b>`
                } else {
                    latestdiv.innerHTML = `<b>Correct! <code>${lastGuess.escapeHtml()}</code> is ${daily} secret regex! Our way of phrasing it (which matches the same words in our dictionary) was <code>${regexpattern.escapeHtml()}</code>.</b>`
                }
                emojiHistory += emoji.regexCorrect
                latestdiv.innerHTML += ` You guessed ${string} and ${regex}.`
                confetti({
                    particleCount: 100,
                    origin: {x: 0},
                    angle: 20,
                    spread: 55,
                    startVelocity: 60,
                })
            } else {
                latestdiv.classList = "complaining"
                const daily = usingDailyRegex
                    ? `for Regexle #${puzzleNumber} `
                    : ""
                latestdiv.innerHTML = `<b>You gave up. The secret regex ${daily}is: <code>${regexpattern.escapeHtml()}</code></b>`
                emojiHistory += emoji.gaveUp
            }
            const pre = document.createElement("pre")
            const prediv = document.createElement("div")
            pre.appendChild(prediv)
            const daily = usingDailyRegex
                ? `Regexle #${puzzleNumber}`
                : `Custom Regexle`
            if (win) {
                prediv.innerHTML = `Solved ${daily} with ${string} and ${regex}!<br>`
            } else {
                prediv.innerHTML = `Did not solve ${daily} (${string} and ${regex}).<br>`
            }
            prediv.innerHTML += `<br>${emojiHistory}<br>`
            prediv.innerHTML += `<br>${location.href} #regexle`
            const copy = document.createElement("button")
            copy.id = "copy"
            copy.innerText = "copy"
            pre.appendChild(copy)
            const indicator = document.createElement("span")
            indicator.id = "copy-indicator"
            copy.appendChild(indicator)

            copy.addEventListener("click", () => {
                try {
                    writeClipboardText(prediv.innerText)
                    indicator.innerText = ` ✅`
                } catch (e) {
                    indicator.innerText = ` ❌`
                }
            })
            latestdiv.appendChild(pre)
        }
    },
    false,
)

function couldBeRegex(string) {
    try {
        new RegExp(string)
        return string.match(/[\[\]\(\)\{\}\|\?\*\+\.\$\^\\]/)
    } catch (e) {
        return false
    }
}

async function writeClipboardText(text) {
    try {
        await navigator.clipboard.writeText(text)
    } catch (error) {
        console.error(error.message)
    }
}
